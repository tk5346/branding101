<?php

namespace App\Http\Controllers;
use App\Message;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use DB;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('pages.contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
       






        $this->validate($request, [
            'fullname' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'body'=>['required',
            'regex:/(^([a-zA-Z]+)(\d+)?$)/u'],
        ]);
        $message = new Message;
        $message->fullName = $request->input('fullname');
        $message->email = $request->input('email');
        $message->subject = $request->input('subject');
        $message->userIP=$request->ip();
        $message->userAGENT=$request->server('HTTP_USER_AGENT');

        $data =array(
            'fullname'=>$request->input('fullname'),
            'email' => $request->input('email'),
            'subject' => $request->input('subject'),
            'body' => $request->input('body'),
        );
      
        $message->fullName=strip_tags($message->fullName);
        $message->email=strip_tags($message->email);
        $message->body=strip_tags($message->body);
        $message->subject=strip_tags($message->subject);

        $exist=DB::select("select count(email) as count,created_at as a  from messages where  userIP='$message->userIP' and  userAGENT='$message->userAGENT'  and email='$message->email'  group by (a)");
        // =DB::select("select count(email) as count from feedbacks where restaurantID='$value' and  userIP='$feedback->userIP' and userAGENT='$feedback->userAGENT' and email='$feedback->email' and   created_at > '$prev' and created_at < '$curr'");
        if($exist !=null){
            $created_at=Carbon::parse($exist[0]->a);
            $now =Carbon::now();
            if($now->diffInSeconds($created_at)>86400){
               return ;
             }
             else{
                 return view('pages.duplicate');
             }
            
        }
        else{

        


        $googleToken=$request->input('g-recaptcha-response');
        if(strlen($googleToken)>0){

            $client =new Client(); 
            $response=$client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params'=> array(
                'secret'=>'6LelSHUUAAAAADl07LuZp02zQj8uNMtk3N1u-Mp8',
                'response'=>$googleToken

            )
            
            ]);
           $result = json_decode( $response->getBody()->getContents());
           if($result->success){


                $message->save();
                return redirect('/success');
           
            }
            else
            {
                return back();
            }
        }
        else{
            
            return back();
        }
        
    }//else  for check 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return view('pages.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return view('pages.404');
    }
}
