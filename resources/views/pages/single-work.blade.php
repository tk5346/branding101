@extends('layouts.base')
@section('content')

  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-folder-outline large-icon"></i>
        <h1>OUR WORKS</h1>
        <hr>
      </div>
      <div class="col-lg-4 col-md-4">
        <h3>Offscreen Project</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->

  </div>

  <!-- Carousel
    ================================================== -->
  <div class="section-works">
    <div class="container aligncenter">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <img src={{asset('css/img/works/off01.jpg')}} alt="First slide">
          </div>
          <div class="item">
            <img src={{asset('css/img/works/off02.jpg')}} alt="Second slide">
          </div>
          <div class="item">
            <img src={{asset('css/img/works/off03.jpg')}} alt="Third slide">
          </div>
        </div>
      </div>
      <!-- /.carousel -->
    </div>
  </div>

  <div class="container">
    <div class="row mt presentation">
      <div class="col-lg-4 col-md-4">
        <h3>Project Information</h3>
      </div>
      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
        <p><br/><a class="btn btn-lg btn-theme">Live Version</a></p>
      </div>
      <div class="col-lg-4 col-md-4">
        <p><b>Date:</b> June 2014</p>
        <p>
          <b>Category:</b>
          <pl>Print, Design</pl><br/>
          <b>Client:</b>
          <pl>Offscreen Magazine</pl>
        </p>
        <p><b>Lead Designer:</b>
          <pl>Marcel Newman</pl>
        </p>
        <p><img src={{asset('css/img/team/3.jpg')}} class="img-circle" height="70px" width="70px;"></p>
      </div>
    </div>
    <!-- /row -->
  </div>



 

</body>
</html>
@endsection