@extends('layouts.base')
@section('content')

  <section id="photostack-1" class="photostack photostack-start">
    <div>
      <figure>
        <a href="#" class="photostack-img"><img src= {{asset('css/img/1.jpg')}} alt="img01"/></a>
        <figcaption>
          <h2 class="photostack-title">Serenity Beach</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/2.jpg')}} alt="img02"/></a>
        <figcaption>
          <h2 class="photostack-title">Happy Days</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/3.jpg')}} alt="img03"/></a>
        <figcaption>
          <h2 class="photostack-title">Beautywood</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/4.jpg')}} alt="img04"/></a>
        <figcaption>
          <h2 class="photostack-title">Heaven of time</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/5.jpg')}} alt="img05"/></a>
        <figcaption>
          <h2 class="photostack-title">Speed Racer</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/6.jpg')}} alt="img06"/></a>
        <figcaption>
          <h2 class="photostack-title">Forever this</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/7.jpg')}} alt="img07"/></a>
        <figcaption>
          <h2 class="photostack-title">Lovely Green</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/8.jpg')}} alt="img08"/></a>
        <figcaption>
          <h2 class="photostack-title">Wonderful</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/9.jpg')}} alt="img09"/></a>
        <figcaption>
          <h2 class="photostack-title">Love Addict</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/10.jpg')}} alt="img10"/></a>
        <figcaption>
          <h2 class="photostack-title">Friendship</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/11.jpg')}} alt="img11"/></a>
        <figcaption>
          <h2 class="photostack-title">White Nights</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/12.jpg')}} alt="img12"/></a>
        <figcaption>
          <h2 class="photostack-title">Serendipity</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/13.jpg')}} alt="img13"/></a>
        <figcaption>
          <h2 class="photostack-title">Pure Soul</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/14.jpg')}} alt="img14"/></a>
        <figcaption>
          <h2 class="photostack-title">Winds of Peace</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/15.jpg')}} /></a>
        <figcaption>
          <h2 class="photostack-title">Shades of blue</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/16.jpg')}} alt="img16"/></a>
        <figcaption>
          <h2 class="photostack-title">Lightness</h2>
        </figcaption>
      </figure>
    </div>
  </section>

  <div class="container">
    <div class="row mt presentation">
      <img class="camera aligncenter" src={{asset('css/img/camera.png')}} alt="">
      <h1 class="centered">WE ARE POLAROYD</h1>
      <hr>

      <div class="col-lg-4 col-md-4">
        <h3>About Our Company</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->
  </div>

  <section id="slides">
    <ul class="slides-container">
      <li>
        <img src={{asset('css/img/slide-1.jpg')}} alt="">
      </li>
      <li>
        <img src={{asset('css/img/slide-2.jpg')}} alt="">
      </li>
      <li>
        <img src={{asset('css/img/slide-3.jpg')}} alt="">
      </li>
    </ul>
    <div id="bannertext">
      <h3>A Truly Handsome Bootstrap 3 Theme</h3>
      <h1>HIGH QUALITY</h1>
    </div>
    <!-- end:Banner text -->
    <nav class="slides-navigation">
      <a href="index2.html#" class="next">
            <i class="fa fa-angle-right"></i>
            </a>
      <a href="index2.html#" class="prev">
            <i class="fa fa-angle-left"></i>
            </a>
    </nav>
  </section>

  <div class="container">
    <ul class="row hidden-sm clients mt2">
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="First Logo">
                    <img alt="" src={{asset('css/img/logos/logo-1.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Second Logo">
                    <img alt="" src={{asset('css/img/logos/logo-2.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Third Logo">
                    <img alt="" src={{asset('css/img/logos/logo-3.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Fourth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-4.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Fifth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-5.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Sixth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-6.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Seventh Logo">
                    <img alt="" src={{asset('css/img/logos/logo-7.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Eighth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-8.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
    </ul>
    <!-- end:Clients section -->
  </div>




  @endsection

</body>
</html>
