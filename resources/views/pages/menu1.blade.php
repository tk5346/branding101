@extends('layouts.base')
@section('content')
 
  <section id="photostack-1" class="photostack photostack-start">
    <div>
      <figure>
        <a href="#" class="photostack-img"><img src= {{asset('css/img/1.jpg')}} alt="img01"/></a>
        <figcaption>
          <h2 class="photostack-title">Serenity Beach</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/2.jpg')}} alt="img02"/></a>
        <figcaption>
          <h2 class="photostack-title">Happy Days</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/3.jpg')}} alt="img03"/></a>
        <figcaption>
          <h2 class="photostack-title">Beautywood</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/4.jpg')}} alt="img04"/></a>
        <figcaption>
          <h2 class="photostack-title">Heaven of time</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/5.jpg')}} alt="img05"/></a>
        <figcaption>
          <h2 class="photostack-title">Speed Racer</h2>
        </figcaption>
      </figure>
      <figure>
        <a href="#" class="photostack-img"><img src={{asset('css/img/6.jpg')}} alt="img06"/></a>
        <figcaption>
          <h2 class="photostack-title">Forever this</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/7.jpg')}} alt="img07"/></a>
        <figcaption>
          <h2 class="photostack-title">Lovely Green</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/8.jpg')}} alt="img08"/></a>
        <figcaption>
          <h2 class="photostack-title">Wonderful</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/9.jpg')}}  alt="img09"/></a>
        <figcaption>
          <h2 class="photostack-title">Love Addict</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/10.jpg')}}  alt="img10"/></a>
        <figcaption>
          <h2 class="photostack-title">Friendship</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/11.jpg')}}  alt="img11"/></a>
        <figcaption>
          <h2 class="photostack-title">White Nights</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/12.jpg')}}  alt="img12"/></a>
        <figcaption>
          <h2 class="photostack-title">Serendipity</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/13.jpg')}} alt="img13" /></a>
        <figcaption>
          <h2 class="photostack-title">Pure Soul</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/14.jpg')}}  alt="img14"/></a>
        <figcaption>
          <h2 class="photostack-title">Winds of Peace</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/15.jpg')}}  alt="img15"/></a>
        <figcaption>
          <h2 class="photostack-title">Shades of blue</h2>
        </figcaption>
      </figure>
      <figure data-dummy>
        <a href="#" class="photostack-img"><img src={{asset('css/img/16.jpg')}}  alt="img16"/></a>
        <figcaption>
          <h2 class="photostack-title">Lightness</h2>
        </figcaption>
      </figure>
    </div>
  </section>

  <div class="container">
    <div class="row mt presentation">
      <img class="camera aligncenter" src={{asset('css/img/camera.png')}} alt="">
      <h1 class="centered">WE ARE POLAROYD</h1>
      <hr>

      <div class="col-lg-4 col-md-4">
        <h3>About Our Company</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->

    <div class="row mt2">
      <div class="col-md-12 centered">
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-heart-outline large-icon"></i>
          <h4>GRAPHICS DESIGN</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-monitor-outline large-icon"></i>
          <h4>WEB DESIGN</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-pie-outline large-icon"></i>
          <h4>CODING</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-plus-empty large-icon"></i>
          <h4>APPLICATION</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-wineglass-outline large-icon"></i>
          <h4>WEB DEVELOPMENT</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-lightbulb-outline large-icon"></i>
          <h4>PRODUCTION</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
      </div>
    </div>

    <div class="row mt">
      <div id="filters-container" class="cbp-l-filters-alignRight container col-sm-12">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
          All
          <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".identity" class="cbp-filter-item">
          Identity
          <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".web-design" class="cbp-filter-item">
          Web Design
          <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".graphic" class="cbp-filter-item">
          Graphic
          <div class="cbp-filter-counter"></div>
        </div>
      </div>
      <!-- end:Filter -->
    </div>
  </div>
  <div id="grid-container" class="cbp-l-grid-fullScreen">
    <ul>
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/1.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/1.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item web-design">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/2.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/2.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/3.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/3.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/4.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/4.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/5.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/5.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/6.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/6.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item web-design">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/7.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/7.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href=  {{ asset('css/img/works/8.jpg')}} >
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/8.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
    </ul>
  </div>

  <ul class="row hidden-sm clients mt2">
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="First Logo">
                    <img alt="" src={{asset('css/img/logos/logo-1.png')}} title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Second Logo">
                    <img alt="" src={{asset('css/img/logos/logo-2.png')}} title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Third Logo">
                    <img alt="" src={{asset('css/img/logos/logo-3.png')}}  title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3 brd-btm">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Fourth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-4.png')}}  title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Fifth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-5.png')}}  title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Sixth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-6.png')}}  title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Seventh Logo">
                    <img alt="" src={{asset('css/img/logos/logo-7.png')}}  title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
    <li class="banner-wrap col-sm-3">
      <figure class="featured-thumbnail">
        <a href="index.html#" data-toggle="tooltip" title="Eighth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-8.png')}}  title="" class="img-responsive">
                    </a>
      </figure>
    </li>
    <!-- .banner-wrap (end) -->
  </ul>
  <!-- end:Clients section -->
  </div>



</body>
</html>
@endsection