@extends('layouts.base')
@section('content')

  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-chatbubble-outline large-icon"></i>
        <h1>BLOG POST</h1>
        <hr>
      </div>
    </div>
    <!-- /row -->
  </div>

  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 centered presentation">
        <img class="img-circle" src={{asset('css/img/team/1.jpg')}} height="90px" width="90px" alt="">
        <h4>Deborah Stern</h4>
        <p class="sp">
          <gr>12 August, 2014</gr>
        </p>
        <p class="sp">
          <pl>Design - Web Development<br/>2 Comments</pl>
        </p>

      </div>

      <div class="col-lg-7 col-md-7 blog-post">
        <h5><a href="blog.html">BACK TO BLOG</a></h5>
        <h2>Workspaces Inspiration. The Ultimate List By Debbie</h2>
        <p class="lead sep">We may define a food to be any substance which will repair the functional waste of the body, increase its growth, or maintain the heat, muscular, and nervous energy.</p>
        <p>In its most comprehensive sense, the oxygen of the air is a food; as although it is admitted by the lungs, it passes into the blood, and there re-acts upon the other food which has passed through the stomach. It is usual, however, to <a href="#">restrict the term food</a>          to such nutriment as enters the body by the intestinal canal. Water is often spoken of as being distinct from food, but for this there is no sufficient reason.</p>
        <p>Many <a href="#">popular writers</a> have divided foods into flesh-formers, heat-givers, and bone-formers. Although attractive from its simplicity, this classification will not bear criticism. Flesh-formers are also heat-givers. Only a portion
          of the mineral matter goes to form bone.</p>
        <h3 class="sep">Flesh-formers, heat-givers, and bone-formers</h3>
        <p>Mineral matter is quite as necessary for plant as for animal life, and is therefore present in all food, except in the case of some highly-prepared ones, such as sugar, starch and oil. Children require a good proportion of calcium phosphate for
          the growth of their bones, whilst adults require less. The outer part of the grain of cereals is the richest in mineral constituents, white flour and rice are deficient. Wheatmeal and oatmeal are especially recommended for the quantity of phosphates
          and other salts contained in them. Mineral matter is necessary not only for the bones but for every tissue of the body.</p>
        <p>The loss on soaking in cold water, unless the water is preserved, is seen to be considerable. The split lentils, having had the protecting skin removed, lose most. In every case the ash contained a good deal of phosphate and lime. Potatoes are
          rich in important potash salts; by boiling a large quantity is lost, by steaming less and by baking in the skins, scarcely any. The flavour is also much better after baking.</p>
        <h3 class="sep2">2 Comments</h3>
        <hr>
      </div>

    </div>

    <div class="row">
      <div class="col-lg-7 col-md-7 col-lg-offset-4">
        <div class="comments">
          <img class="img-circle" src={{asset('css/img/team/3.jpg')}} height="60px" width="60px">
          <h4>Mark Jacobson - August 13, 2014</h4>
          <p><a href="#">REPLY</a></p>
          <p>Proteids or Albuminoids are frequently termed flesh-formers. They are composed of nitrogen, carbon, hydrogen, oxygen, and a small quantity of sulphur, and are extremely complex bodies.</p>
        </div>

        <div class="comments">
          <img class="img-circle" src={{asset('css/img/team/4.jpg')}} height="60px" width="60px">
          <h4>Maggie Shaw - August 13, 2014</h4>
          <p><a href="#">REPLY</a></p>
          <p>Their chief function is to form flesh in the body; but without previously forming it, they may be transformed into fat or merely give rise to heat. They form the essential part of every living cell.</p>
        </div>
      </div>
    </div>
  </div>

</body>
</html>
@endsection