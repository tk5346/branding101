@extends('layouts.base')
@section('content')


  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-paperplane-outline large-icon"></i>
        <h1>CONTACT US</h1>
        <hr>
      </div>
      <div class="col-lg-4 col-md-4">
        <h3>Polaroyd Agency</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>
          <b>Address:</b>
          <pl>902 Main Street </pl><br/>
          <b>Telephone:</b>
          <pl>(598) 123-4567</pl>
        </p>
        <p><b>Email:</b>
          <pl>support@polaroyd.com</pl>
        </p>
      </div>
    </div>
    <!-- /row -->

  </div>

  <div class="container">
    <div class="row presentation">
      <div class="col-lg-4 col-md-4">
      </div>
      <div class="col-lg-8 col-md-8 contact-form">
        <form class="contact-form php-mail-form" role="form" action="contactform/contactform.php" method="POST">

            <div class="form-group">
              <input type="name" name="name" class="form-control" id="contact-name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" >
              <div class="validate"></div>
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" id="contact-email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
              <div class="validate"></div>
            </div>
            <div class="form-group">
              <input type="text" name="subject" class="form-control" id="contact-subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
              <div class="validate"></div>
            </div>

            <div class="form-group">
              <textarea class="form-control" name="message" id="contact-message" placeholder="Your Message" rows="5" data-rule="required" data-msg="Please write something for us"></textarea>
              <div class="validate"></div>
            </div>

            <div class="loading"></div>
            <div class="error-message"></div>
            <div class="sent-message">Your message has been sent. Thank you!</div>

            <div class="form-send">
              <button type="submit" class="btn btn-lg btn-transparent">Send Message</button>
            </div>

          </form>
      </div>
    </div>
    <!-- /row -->
  </div>




</body>
</html>
@endsection