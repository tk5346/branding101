@extends('layouts.base')
@section('content')


  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-cog-outline large-icon"></i>
        <h1>SERVICES</h1>
        <hr>
      </div>
      <div class="col-lg-4 col-md-4">
        <h3>Our Methodology</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->

    <div class="row mt2">
      <div class="col-md-12 centered">
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-heart-outline large-icon"></i>
          <h4>GRAPHICS DESIGN</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-monitor-outline large-icon"></i>
          <h4>WEB DESIGN</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-pie-outline large-icon"></i>
          <h4>CODING</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-plus-empty large-icon"></i>
          <h4>APPLICATION</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-wineglass-outline large-icon"></i>
          <h4>WEB DEVELOPMENT</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
        <div class="col-md-4 services-bordered">
          <i class="icon ion-ios7-lightbulb-outline large-icon"></i>
          <h4>PRODUCTION</h4>
          <p>Sed in porttitor dolor. Sed eleifend vulputate nulla, congue iaculis arcu mattis porta.</p>
        </div>
      </div>
    </div>

    <div class="row mt2">
      <div class="col-md-6 col-md-offset-3 centered presentation">
        <h3>You Should Contact Us</h3>
        <hr>
        <a href="contact.html" class="btn btn-lg btn-theme">Get A Quote</a>

      </div>
    </div>
  </div>




</body>
</html>
@endsection