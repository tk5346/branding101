@extends('layouts.base')
@section('content')


  <div class="container">
    <div class="row mt3 presentation centered">
      <i class="icon ion-ios7-briefcase-outline large-icon"></i>
      <h1 class="centered">OUR CLIENTS</h1>
      <hr>
    </div>
    <!-- /row -->
  </div>

  <div class="container">
    <ul class="row hidden-sm clients">
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="First Logo">
                    <img alt="" src={{asset('css/img/logos/logo-1.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Second Logo">
                    <img alt="" src={{asset('css/img/logos/logo-2.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Third Logo">
                    <img alt="" src={{asset('css/img/logos/logo-3.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3 brd-btm">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Fourth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-4.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Fifth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-5.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Sixth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-6.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Seventh Logo">
                    <img alt="" src={{asset('css/img/logos/logo-7.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
      <li class="banner-wrap col-sm-3">
        <figure class="featured-thumbnail">
          <a href="index.html#" data-toggle="tooltip" title="Eighth Logo">
                    <img alt="" src={{asset('css/img/logos/logo-8.png')}} title="" class="img-responsive">
                    </a>
        </figure>
      </li>
      <!-- .banner-wrap (end) -->
    </ul>
    <!-- end:Clients section -->
  </div>

  <div class="container presentation">
    <div class="row">
      <div class="col-lg-4 col-md-4">
        <h3>Testimonials</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
        <p>
          <pl>FRANCIS WHITE</pl> - Google Inc.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
        <p>
          <pl>PAUL BROWN</pl> - Google Inc.</p>
      </div>

      <div class="col-lg-4 col-md-4">
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
        <p>
          <pl>MARK TWAIN</pl> - Apple Co.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
        <p>
          <pl>FRANK FORD</pl> - IBM</p>
      </div>

    </div>
  </div>



</body>
</html>
@endsection