

@extends('layouts.base')
@section('content')
  <div class="container">
    <div class="row mt3 presentation centered">
      <i class="icon ion-ios7-bookmarks-outline large-icon"></i>
      <h1 class="centered">ABOUT US</h1>
      <hr>
    </div>
    <div class="row presentation">
      <div class="col-lg-4 col-md-4">
        <h3>About Our Company</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->

  </div>

  <section id="slides">
    <ul class="slides-container">
      <li>
        <img src={{asset('css/img/slide-4.jpg')}} alt="">
      </li>
      <li>
        <img src={{asset('css/img/slide-5.jpg')}} alt="">
      </li>
      <li>
        <img src={{asset('css/img/slide-6.jpg')}} alt="">
      </li>
    </ul>
    <div id="bannertext">
      <h3>Some Shots From Our Place</h3>
      <h1>OUR BUNKER</h1>
    </div>
  </section>



  <div class="container mt2">
    <div class="row presentation">
      <div class="col-lg-4 col-md-4">
        <h3>Company Profile</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <h3>112</h3>
        <p>TOTAL EMPLOYEES</p>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <h3>1400+</h3>
        <p>CLIENTS</p>
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>

      <div class="col-lg-4 col-md-4">
      </div>

      <div class="col-lg-4 col-md-4">
        <h3>$ 2.000.000</h3>
        <p>REVENUE</p>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <h3>8</h3>
        <p>YEARS OF EXPERIENCE</p>
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->
  </div>

  <div class="container mt">
    <div class="row presentation">
      <div class="col-lg-1 col-md-1 centered">
        <i class="icon ion-ios7-flag-outline large-icon"></i>
        <h3>2006</h3>
      </div>
      <div class="col-lg-3 col-md-3">
        <h4>OUR BEGINNING</h4>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>
      <div class="col-lg-1 col-md-1 centered">
        <i class="icon ion-ios7-pricetag-outline large-icon"></i>
        <h3>2009</h3>
      </div>
      <div class="col-lg-3 col-md-3">
        <h4>OUR FIRST MILLION</h4>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>
      <div class="col-lg-1 col-md-1 centered">
        <i class="icon ion-ios7-people-outline large-icon"></i>
        <h3>2013</h3>
      </div>
      <div class="col-lg-3 col-md-3">
        <h4>100 EMPLOYEES</h4>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>
    </div>
    <!-- /row -->
  </div>

 



</body>
</html>
@endsection