@extends('layouts.base')
@section('content')
  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-pricetag-outline large-icon"></i>
        <h1>PRICING</h1>
        <hr>
      </div>
      <div class="col-lg-4 col-md-4">
        <h3>Our Affordable Prices</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->
  </div>

  <div class="container mt">
    <div class="row pricing">

      <div class="col-lg-3 col-md-3">
        <h3>BASIC</h3>
        <div class="num">
          <sup>$</sup>
          <ins>0</ins>
        </div>
        <h5>Per Month</h5>
        <p class="mt">
          3 Bootstrap Templates<br/> 1 Domain License<br/> Free Support<br/> -
        </p>
        <p class="mt">
          <a href="#" class="btn btn-lg btn-transparent">Try It Free</a>
        </p>
      </div>

      <div class="col-lg-3 col-md-3 highlight border">
        <h3>PERSONAL</h3>
        <div class="num">
          <sup>$</sup>
          <ins>20</ins>
        </div>
        <h5>Per Month</h5>
        <p class="mt">
          10 Bootstrap Templates<br/> 5 Domain License<br/> Free Support<br/> 5 GB of Space
        </p>
        <p class="mt">
          <a href="#" class="btn btn-lg btn-theme">Get Started</a>
        </p>
      </div>

      <div class="col-lg-3 col-md-3">
        <h3>BUSINESS</h3>
        <div class="num">
          <sup>$</sup>
          <ins>80</ins>
        </div>
        <h5>Per Month</h5>
        <p class="mt">
          50 Bootstrap Templates<br/> 25 Domain License<br/> Free Support<br/> 25 GB of Space
        </p>
        <p class="mt">
          <a href="#" class="btn btn-lg btn-transparent">Get Started</a>
        </p>
      </div>

      <div class="col-lg-3 col-md-3 highlight">
        <h3>PREMIUM</h3>
        <div class="num">
          <sup>$</sup>
          <ins>120</ins>
        </div>
        <h5>Per Month</h5>
        <p class="mt">
          100 Bootstrap Templates<br/> 50 Domain License<br/> Free Support<br/> 500 GB of Space
        </p>
        <p class="mt">
          <a href="#" class="btn btn-lg btn-transparent">Get Started</a>
        </p>
      </div>

    </div>
  </div>





</body>
</html>
@endsection