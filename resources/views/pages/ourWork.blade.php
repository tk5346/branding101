@extends('layouts.base')
@section('content')
<div class="container">
    <div class="row mt3 presentation centered">
      <i class="icon ion-ios7-monitor-outline large-icon"></i>
      <h1 class="centered">OUR WORKS</h1>
      <hr>
    </div>
    <!-- /row -->

    <div class="row centered">
      <div id="filters-container" class="cbp-l-filters-alignRight container col-sm-12">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
          All
          <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".identity" class="cbp-filter-item">
          Identity
          <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".web-design" class="cbp-filter-item">
          Web Design
          <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".graphic" class="cbp-filter-item">
          Graphic
          <div class="cbp-filter-counter"></div>
        </div>
      </div>
      <!-- end:Filter -->
    </div>
  </div>

  <div id="grid-container" class="cbp-l-grid-fullScreen">
    <ul>
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href={{asset('css/img/works/1.jpg')}}>
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/1.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item web-design">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/2.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/2.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/3.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/3.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/4.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/4.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/5.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/5.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/6.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/6.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item web-design">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/7.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/7.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/8.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/8.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/9.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/9.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item graphic">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/11.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/11.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item web-design">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href="img/works/10.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/10.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
      <li class="cbp-item identity">
        <a class="cbp-caption cbp-lightbox" data-title="Image" href=img/works/12.jpg">
          <div class="cbp-caption-defaultWrap">
            <img src={{asset('css/img/works/12.jpg')}} alt="" class="img-responsive">
          </div>
          <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
              <div class="cbp-l-caption-body">
                <div class="cbp-l-caption-title">Image LightBox</div>
                <div class="cbp-l-caption-desc">by John Doe</div>
              </div>
            </div>
          </div>
        </a>
      </li>
      <!-- end:Item -->
    </ul>
  </div>

  <div class="container mt2">
    <div class="row presentation">
      <div class="col-lg-1 col-md-1 centered">
        <h2>119</h2>
      </div>
      <div class="col-lg-3 col-md-3">
        <h4>PHOTOGRAPHY</h4>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-1 col-md-1 centered">
        <h2>28</h2>
      </div>
      <div class="col-lg-3 col-md-3">
        <h4>WEB DESIGN</h4>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-1 col-md-1 centered">
        <h2>227</h2>
      </div>
      <div class="col-lg-3 col-md-3">
        <h4>LOGOS</h4>
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>
    </div>
    <!-- /row -->

  </div>




  @endsection

</body>
</html>
