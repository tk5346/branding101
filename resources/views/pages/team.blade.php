@extends('layouts.base')
@section('content')

  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-people-outline large-icon"></i>
        <h1>OUR TEAM</h1>
        <hr>
      </div>
      <div class="col-lg-4 col-md-4">
        <h3>Cool People</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Mellentesque habitant morbi tristique senectus et netus et malesuada famesac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Dummy text of the printing and typesetting.</p>
      </div>
    </div>
    <!-- /row -->

    <div class="row mt2 aligncenter">
      <div class="col-lg-3 col-md-3">
        <!-- normal -->
        <div class="ih-item circle effect1">
          <a href="#">
            <div class="spinner"></div>
            
            <div class="img"><img src={{asset('css/img/team/1.jpg')}} alt="img"></div>
            <h4 class="centered">Rosie Sanders</h4>
            <div class="info">
              <div class="info-back">
                <h3>Rosie</h3>
                <p>Description goes here</p>
              </div>
            </div>
          </a>
        </div>
        <!-- end normal -->
      </div>
      <div class="col-lg-3 col-md-3">
        <!-- normal -->
        <div class="ih-item circle effect1">
          <a href="#">
            <div class="spinner"></div>
            <div class="img"><img src={{asset('css/img/team/2.jpg')}} alt="img"></div>
            <h4 class="centered">Mark Twain</h4>
            <div class="info">
              <div class="info-back">
                <h3>Mark</h3>
                <p>Description goes here</p>
              </div>
            </div>
          </a>
        </div>
        <!-- end normal -->
      </div>
      <div class="col-lg-3 col-md-3">
        <!-- normal -->
        <div class="ih-item circle effect1">
          <a href="#">
            <div class="spinner"></div>
            <div class="img"><img src={{asset('css/img/team/3.jpg')}} alt="img"></div>
            <h4 class="centered">Phillip Sirer</h4>
            <div class="info">
              <div class="info-back">
                <h3>Phillip</h3>
                <p>Description goes here</p>
              </div>
            </div>
          </a>
        </div>
        <!-- end normal -->
      </div>
      <div class="col-lg-3 col-md-3">
        <!-- normal -->
        <div class="ih-item circle effect1">
          <a href="#">
            <div class="spinner"></div>
            <div class="img"><img src={{asset('css/img/team/4.jpg')}} alt="img"></div>
            <h4 class="centered">Deborah Shue</h4>
            <div class="info">
              <div class="info-back">
                <h3>Deborah</h3>
                <p>Description goes here</p>
              </div>
            </div>
          </a>
        </div>
        <!-- end normal -->
      </div>

    </div>
  </div>

</body>
</html>
@endsection