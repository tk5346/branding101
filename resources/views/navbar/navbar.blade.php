 <!-- Custom navbar -->
 <div class="custom-navbar"><a class="fm-button" href="#">Menu</a></div>
 <div class="sidemenu">
   <ul class="fm-first-level">
     <li><a href="#menu1">Main Menu</a></li>
     <li><a href="#menu2">Pages Menu</a></li>
     <li><a href="#menu3">Blog Menu</a></li>
   </ul>
   <nav id="menu1" class="active">
     <a href="{{ url('/') }}">
           <span class="subtitle">Basic Index. Polaroid header, services, portfolio, clients logos.</span>
           Main Index Version
         </a>
     <a href="{{ url('/index2') }}">
           <span class="subtitle">Basic Index with slider. Some objects are removed. Clean Layout.</span>
           Main Index Variation
         </a>
     <a href="{{ url('/ourWork') }}">
           <span class="subtitle">Portfolio section. Grid style.</span>
           Our Works
         </a>
     <a href="{{ url('/about') }}">
           <span class="subtitle">More information about our agency. Simple & elegant.</span>
           About Us
         </a>
     <a href="{{ url('/services') }}">
           <span class="subtitle">List of services provided by us.</span>
           Services
         </a>
     <a href="{{ url('/contact') }}">
           <span class="subtitle">We are kind people and love to speak with our clients.</span>
           Contact Us
         </a>
   </nav>
   <nav id="menu2">
     <a href="{{ url('/team') }}">
           <span class="subtitle">We are an awesome team. Take a look.</span>
           Meet The Team
         </a>
     <a href="{{ url('/swork') }}">
           <span class="subtitle">Single work page. Information about a project.</span>
           Single Work
         </a>
     <a href="{{ url('/clients') }}">
           <span class="subtitle">List of our clients and some cool testimonials.</span>
           Our Clients
         </a>
     <a href="{{ url('/pricing') }}">
           <span class="subtitle">A pricing list. Show your prices with style.</span>
           Pricing
         </a>
     <a href="{{ url('/404') }}">
           <span class="subtitle">Sometimes errors happens.</span>
           404
         </a>
   </nav>
   <nav id="menu3">
     <a href="{{ url('/blog') }}">
           <span class="subtitle">Main blog layout list.</span>
           Blog List
         </a>
  
   </nav>
 </div>
 