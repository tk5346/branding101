<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Polaroyd - Bootstrap Agency Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href={{asset('css/img/favicon.png')}} rel="icon">

  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700&subset=latin,cyrillic-ext" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href={{asset('lib/bootstrap/css/bootstrap.min.css')}} rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href={{asset('lib/font-awesome/css/font-awesome.min.css')}} rel="stylesheet">
  <link href={{asset('lib/ionicons/css/ionicons.min.css')}} rel="stylesheet">
  <link href={{asset('lib/photostack/photostack.css')}} rel="stylesheet">
  <link href={{asset('lib/fullpage-menu/fullpage-menu.css')}} rel="stylesheet">
  <link href={{asset('lib/cubeportfolio/cubeportfolio.css')}} rel="stylesheet">
  <link href={{asset('lib/superslides/superslides.css')}} rel="stylesheet">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Main Stylesheet File -->
  <link href={{asset('css/app.css')}} rel="stylesheet">

  <!-- =======================================================
    Template Name: Polaroyd
    Template URL: https://templatemag.com/polaroyd-bootstrap-agency-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>

    @include('navbar.navbar')
    <main class="py-4">
        {{-- @include('pages.validate') --}}
        @yield('content')
    </main>
    @include('footer.upperFooter')
    @include('footer.footer')
    
  <!-- JavaScript Libraries -->
  <script src={{asset('lib/jquery/jquery.min.js')}}></script>
  <script src={{asset('lib/modernizr/modernizr.min.js')}}></script>
  <script src={{asset('lib/bootstrap/js/bootstrap.min.js')}}></script>
  <script src={{asset('lib/php-mail-form/validate.js')}}></script>
  <script src={{asset('lib/easing/easing.min.js')}}></script>
  <script src={{asset('lib/cubeportfolio/cubeportfolio.js')}}></script>
  <script src={{asset('lib/classie/classie.js')}}></script>
  <script src={{asset('lib/fullpage-menu/fullpage-menu.js')}}></script>
  <script src={{asset('lib/photostack/photostack.js')}}></script>
  <script src={{asset('lib/superslides/superslides.js')}} ></script>
  <script>
    $(function () {
       $('#contactForm').submit(function(event){
           var verified= grecaptcha.getResponse();
           if(verified.length===0){
               event.preventDefault();

           }
            
       });
    });
    // anonymousForm
   
</script>

  <!-- Template Main Javascript File -->
  <script src={{asset('js/main.js')}}></script>
</body>
</html>