@extends('layouts.base')
@section('content')


  <div class="container">
    <div class="row mt3 presentation">
      <div class="centered">
        <i class="icon ion-ios7-paperplane-outline large-icon"></i>
        <h1>CONTACT US</h1>
        <hr>
      </div>
      <div class="col-lg-4 col-md-4">
        <h3>Polaroyd Agency</h3>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>Dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since, when an unknown ristique senectus et netus.</p>
      </div>

      <div class="col-lg-4 col-md-4">
        <p>
          <b>Address:</b>
          <pl>902 Main Street </pl><br/>
          <b>Telephone:</b>
          <pl>(598) 123-4567</pl>
        </p>
        <p><b>Email:</b>
          <pl>support@polaroyd.com</pl>
        </p>
      </div>
    </div>
    <!-- /row -->

  </div>

  <div class="container">
    <div class="row presentation">
      <div class="col-lg-4 col-md-4">
      </div>
      <div class="col-lg-8 col-md-8 contact-form">
        {{-- <form class="contact-form php-mail-form" role="form" action="contactform/contactform.php" method="POST"> --}}
        {!! Form::open(['action' => 'ContactController@store', 'method' => 'POST' ,'id' => 'contactForm']) !!}
                
                <div class="form group">
                    {{Form::email('email','',['data-constraints'=>'@Email','id' =>'contact-email','class' => 'form-control', 'placeholder' => 'Your Email','data-rule'=>"minlen:4",'data-msg'=>"Please enter a valid email", ])}}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form group">
                    {{Form::text('fullname','',['data-constraints'=>'@Required','id' =>'contact-name','class' => 'form-control', 'placeholder' => 'Your Name','data-rule'=>"minlen:4",'data-msg'=>"Please enter at least 4 chars", ])}}
                    {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
               </div>
               <div class="form group">
                    {{Form::text('subject','',['data-constraints'=>'@Required','id' =>'contact-subject','class' => 'form-control', 'placeholder' => 'Your Subject','data-rule'=>"minlen:4",'data-msg'=>"Please enter at least 8 chars of subject", ])}}
                    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
               </div>
              
               <div class="form-group">
                    {{Form::textarea('body', '', [ 'data-constraints'=>'@Required','style'=>'height:auto','id' =>'contact-message','class' => 'form-control', 'placeholder' => 'Your Message', 'rows'=>"5"])}}
                    {!! $errors->first('body', '<p class="help-block">:message</p>') !!}
            </div>

            {{-- <div class="loading"></div>
            <div class="error-message"></div>
            <div class="sent-message">Your message has been sent. Thank you!</div> --}}
            {{-- <div class="range"   style="margin-top:20px; > --}}
            <div class="text-xs-center">
                <div class="g-recaptcha" data-sitekey="6LelSHUUAAAAAIWNCEON2f52kJb8xaPAX857pqzn" style="margin: auto;"></div>
            </div>
            
                        
            <div class="form-send">
              {{-- <button type="submit" class="btn btn-lg btn-transparent">Send Message</button> --}}
              {{Form::submit('Send Message', ['class'=>'btn btn-lg btn-transparent'])}}
            </div>

          </form>
      </div>
    </div>
    <!-- /row -->
  </div>




</body>
</html>
@endsection