<section id="contact" class="section-bg-color ">
    <div class="container centered ptb">
      <div class="col-md-2 col-md-offset-1">
        <i class="icon ion-ios7-location-outline large-icon"></i>
        <h5>HEAD OFFICE</h5>
        <p>902 Main Street, MVD, UY.</p>
      </div>
      <div class="col-md-2 col-md-offset-2">
        <i class="icon ion-ios7-telephone-outline large-icon"></i>
        <h5>CALL US</h5>
        <p>(598) 123-4567</p>
      </div>
      <div class="col-md-2 col-md-offset-2">
        <i class="icon ion-ios7-paperplane-outline large-icon"></i>
        <h5>QUERIES</h5>
        <p>support@mail.com</p>
      </div>
    </div>
  </section>