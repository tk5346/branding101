<section id="end" class="section-footer">
    <div class="container centered ptb">
      <div class="col-md-6 col-md-offset-3">
        <h3>Polaroyd Template</h3>
        <p>Mellentesque habitant morbi tristique senectus<br/> et netus et malesuada famesac turpis egestas.</p>
        <p class="mt">
          <a href="#"><i class="fa fa-dribbble"></i></a>
          <a href="#"><i class="fa fa-instagram"></i></a>
          <a href="#"><i class="fa fa-twitter"></i></a>
        </p>
        <p>Copyright &copy; - <strong>Polaroyd</strong> All rights reserved.</p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/polaroyd-bootstrap-agency-template/
            Licensing information: https://templatemag.com/license/
          -->
          Created with Polaroyd template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
      </div>
    </div>
  </section>