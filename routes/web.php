<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/',function(){
//     return view('pages.menu');
// });

Route::get('/',function(){
    return view('pages.menu1');
});
// Route::get('//',function(){
//     return view('pages.menu1');
// });
Route::get('/contact',function(){
    return view('pages.contact');
});

Route::get('/404',function(){
    return view('pages.404');
});
Route::get('/services',function(){
    return view('pages.services');
});
Route::get('/ourWork',function(){
    return view('pages.ourWork');
});
Route::get('/about',function(){
    return view('pages.about');
});
Route::get('/team',function(){
    return view('pages.team');
});
Route::get('/swork',function(){
    return view('pages.single-work');
});
Route::get('/pricing',function(){
    return view('pages.pricing');
});
Route::get('/clients',function(){
    return view('pages.clients');
});
Route::get('/blog',function(){
    return view('pages.blog');
});
Route::get('/index2',function(){
    return view('pages.index2');
});
Route::get('/success',function(){
    return view('pages.thanks');
});
// Route::get('/contact1',function(){
//     return view('contact.create');
// });
Route::resource('/contact1','ContactController');
// Route::get('/contact1/create','ContactController@create');

Route::get('{any?}', function ($any) {
    return view('pages.404');
})->where('any', '.*');